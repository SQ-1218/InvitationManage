<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>论坛发帖</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
      <h2>回复信息列表</h2>
    <table border="1" id="ReplyDetailList">
    	<tr>
    		<th>回复内容</th>
    		<th>回复昵称</th>
    		<th>操作</th>
    	</tr>
    	
    	<!--此处显示信息列表-->
        <c:forEach var="ReplyDetail" items="${ReplyDetailList}">
           	 <tr>
	    		<td>${ReplyDetail.content}</td>
	    		<td>${ReplyDetail.author}</td>
	    		<td>${ReplyDetail.createdate}</td>   
	    		<td>
	    		<a href="ReplyDetail/addreply">添加回复 </a> ||
	    		<a href="ReplyDetail/back">返回帖子列表 </a>
	    		</td>  
              </tr>
		</c:forEach>
    </table>
		<input type="hidden" id="totalPageCount" value="${totalPageCount}"/>
		  	<c:import url="rollpage.jsp">
	          	<c:param name="totalCount" value="${totalCount}"/>
	          	<c:param name="currentPageNo" value="${currentPageNo}"/>
	          	<c:param name="totalPageCount" value="${totalPageCount}"/>
           </c:import>
   <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.10.2.min.js"></script>
   <script>
   $(function(){
	$("table tr:even").css("background","yellow");  //隔行变色
	$("table tr:odd").css("background","orange");  //隔行变色
	});	
   </script>
  </body>
</html>
