<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>论坛发帖</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
      <h2>帖子列表</h2>
  <form method="post" action="${pageContext.request.contextPath }/Invitation/list">
  	<span>帖子标题：</span>
  	<input type="text" name="title" id="title">
 	 <input value="搜 索" type="submit" id="searchbutton">
 
    <table border="1" id="InvitationList">
    	<tr>
    		<td>帖子标题</td>
    		<td>帖子摘要</td>
    		<td>作者</td>
    		<td>发布时间</td>
    		<td>操作</td>
    	</tr>
    	
    	<!--此处显示信息列表-->
        <c:forEach var="invitation" items="${InvitationList}">
           	 <tr>
	    		<td>${invitation.title}</td>
	    		<td>${invitation.summary}</td>
	    		<td>${invitation.author}</td>
	    		<td>${invitation.createdate}</td>   
	    		<td>
	    		<a href="${pageContext.request.contextPath }/ReplyDetail/replylist/${invitation.id}">查看回复 </a> <span>||</span> 
	    		<a href="${pageContext.request.contextPath }/ReplyDetail/delete">删除 </a>
	    		</td>
             </tr>
		</c:forEach>
    </table>
		 <input type="hidden" id="totalPageCount" value="${totalPageCount}"/>
		  	<c:import url="rollpage.jsp">
	          	<c:param name="totalCount" value="${totalCount}"/>
	          	<c:param name="currentPageNo" value="${currentPageNo}"/>
	          	<c:param name="totalPageCount" value="${totalPageCount}"/>
           </c:import>
    </form> 
   <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.10.2.min.js"></script>
   <script>
   $(function(){
	$("table tr:even").css("background","yellow");  //隔行变色
	$("table tr:odd").css("background","orange");  //隔行变色
	});	
   </script>
  </body>
</html>
