package cn.until;


public class Page {
	private int pageCount = 0;
	private int pageSize = 5;
	private int totalCount;
	private int currPageNo = 1;


	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		if (pageSize > 0) {
			this.pageSize = pageSize;
		}
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		if (totalCount > 0) {
			this.totalCount = totalCount;
			pageCount = this.totalCount % pageSize == 0 ? (this.totalCount / this.pageSize)
					: (this.totalCount / this.pageSize + 1);
		}
		this.totalCount = totalCount;
	}

	public int getCurrPageNo() {
		if (pageCount == 0) {
			return 0;
		}
		return currPageNo;
	}

	public void setCurrPageNo(int currPageNo) {
		if (currPageNo > 0) {
			this.currPageNo = currPageNo;
		}
	}


}
