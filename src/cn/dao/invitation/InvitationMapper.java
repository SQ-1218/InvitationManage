package cn.dao.invitation;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.entity.Invitation;

public interface InvitationMapper {
	public int getCount(@Param("title") String title);//��ѯ����
	public List<Invitation> getlist(@Param("title") String title,@Param("PageNo") Integer currentPageNo,@Param("PageSize") Integer PageSize);
}
