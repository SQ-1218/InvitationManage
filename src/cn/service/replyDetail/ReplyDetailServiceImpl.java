package cn.service.replyDetail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.dao.replyDetail.ReplyDetailMapper;
import cn.entity.ReplyDetail;
@Service("ReplyDetailService")
public class ReplyDetailServiceImpl implements ReplyDetailService{

	@Autowired
	private ReplyDetailMapper replyDetailMapper;
	@Override
	public int getCount(int invid) {
		// TODO Auto-generated method stub
		return replyDetailMapper.getCount(invid);
	}

	@Override
	public List<ReplyDetail> getlistbyinvid(int invid,
			Integer currentPageNo, Integer PageSize) {
		// TODO Auto-generated method stub
		return replyDetailMapper.getlistbyinvid(invid, currentPageNo, PageSize);
	}

	@Override
	public int add(ReplyDetail replyDetail) {
		// TODO Auto-generated method stub
		return replyDetailMapper.add(replyDetail);
	}

}
