package cn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cn.entity.Invitation;
import cn.entity.ReplyDetail;
import cn.service.replyDetail.ReplyDetailService;
import cn.until.Page;

@Controller
@RequestMapping("/ReplyDetail")
public class ReplyDetailController {
	@Autowired
	private ReplyDetailService replyDetailService;
	
	@RequestMapping(value="/replylist" )//地址
	public String getproviderlist(Model model,@RequestParam(value="invid") int invid,
			@RequestParam(value="pageIndex",required=false) String pageIndex){

		List<ReplyDetail> ReplyDetailList = null;
		//设置页面容量
		int pageSize=4;
		//当前页码
		int currentPageNo=1;

		if(pageIndex!=null){
			try{
				currentPageNo=Integer.valueOf(pageIndex);
			}catch(NumberFormatException e){
				return "error";
			}
		}
		//总数量
		int totalCount =replyDetailService.getCount(invid);
		//总页数
		Page pages= new Page();
		pages.setCurrPageNo(currentPageNo);
		pages.setPageSize(pageSize);
		pages.setTotalCount(totalCount);
		int totalPageCount=pages.getPageCount();
		
		//控制首页和尾页
		if(currentPageNo<1){
			currentPageNo=1;
		}else if(currentPageNo>totalPageCount){
			currentPageNo=totalPageCount;
		}
		ReplyDetailList=replyDetailService.getlistbyinvid(invid, currentPageNo, pageSize);

		model.addAttribute("ReplyDetailList",ReplyDetailList);
		model.addAttribute("invid",invid);
		model.addAttribute("totalPageCount",totalPageCount);
		model.addAttribute("totalCount",totalCount);
		model.addAttribute("currentPageNo",currentPageNo);		
		return "replylist";
	}
}
