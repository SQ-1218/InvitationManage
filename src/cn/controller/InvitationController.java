package cn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cn.entity.Invitation;
import cn.service.invitation.InvitationService;
import cn.until.Page;
@Controller
@RequestMapping("/Invitation")
public class InvitationController {
	@Autowired
	private InvitationService invitationService;
	
	@RequestMapping(value="/list", method=RequestMethod.POST)//地址
	public String getproviderlist(Model model,@RequestParam(value="title",required=false) String title,
			@RequestParam(value="pageIndex",required=false) String pageIndex){

		List<Invitation> InvitationList = null;
		//设置页面容量
		int pageSize=6;
		//当前页码
		int currentPageNo=1;

		if(pageIndex!=null){
			try{
				currentPageNo=Integer.valueOf(pageIndex);
			}catch(NumberFormatException e){
				return "error";
			}
		}
		//总数量
		int totalCount =invitationService.getCount(title);
		//总页数
		Page pages= new Page();
		pages.setCurrPageNo(currentPageNo);
		pages.setPageSize(pageSize);
		pages.setTotalCount(totalCount);
		int totalPageCount=pages.getPageCount();
		
		//控制首页和尾页
		if(currentPageNo<1){
			currentPageNo=1;
		}else if(currentPageNo>totalPageCount){
			currentPageNo=totalPageCount;
		}
		InvitationList=invitationService.getlist(title, currentPageNo, pageSize);

		model.addAttribute("InvitationList",InvitationList);
		model.addAttribute("title",title);
		model.addAttribute("totalPageCount",totalPageCount);
		model.addAttribute("totalCount",totalCount);
		model.addAttribute("currentPageNo",currentPageNo);		
		return "index";
	}
}